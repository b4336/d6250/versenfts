# verseNFTs

This is a toy project which mints whatever Bible verses you like into non-fungible tokens (NFTs). 

The metadata includes:

1. the version of the Bible, 
2. the specific book in the Bible, and 
3. the chapter and verse numbers in the form of x:y-z.